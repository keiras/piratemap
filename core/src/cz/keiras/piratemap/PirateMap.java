package cz.keiras.piratemap;

import java.nio.ByteBuffer;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.CatmullRomSpline;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ScreenUtils;

/*
 * 	---MANDATORY---
 * 	DONE	At least one distinguishable landmass on the map.
 * 	DONE	An X, to mark the treasure.
 * 
 *  ---OPTIONAL---
 * 	PART	Terrain features (forests, hills, mountains)
 * 			Seed based generation (different seed, different map)
 * 	DONE	Believable landmasses (as in, look like islands)
 * 			Decorations (Ships, giant squid, waves)
 * 	DONE	A marked path to follow to the X
 * 			Rivers
 * 			Named Features (e.g the forest of doom, the desert of argh)
 * 			Believable Rivers
 * 			A list of instructions to follow the path
 * 			A list of instructions to find the treasure
 * 			A link to the source code on github or similar
 */

/*
 * Created by Keiras <kryl.martin@centrum.cz>
 */

public class PirateMap extends ApplicationAdapter {
	// Landscape
	Texture map;
	// Fancy shoreline and contour lines
	Texture filter;
	// X mark, path, (possibly more later)
	Texture symbols;
	// Landscape pixmap
	Pixmap pmap;
	Pixmap symbolsPmap;
	// BGM
	Music mapMusic;
	SpriteBatch renderSprite;
	// To make nice curves on path line
	ShapeRenderer path; 
	// To deal with y axis being inverted in some cases
	OrthographicCamera camera;
	
	final int mapW = 1024;
	final int mapH = 768;
	final int waterColor = Color.rgba8888(new Color(20/255f, 143/255f, 184/255f, 1.0f));
	// scale down of initially generated map (to make generator faster and to help smoothing edges)
	final int generatorStep = 5;
	final int oceanShoreline = 11;
	// new height color every contourLineDistance
	final int contourLineDistance = 15;

	// reference points for drawing the path to the treasure
	int[] pathPoints;
	// interpolated points for path line
	Vector2[] points;
	
	// generation is done
	boolean finished;
	// screenshot of generated map has been done
	boolean screenshotDone;
	HeightmapGenerator gen;
	int[][] intmap;
	
	// possibility to stay on the same generator step for several frames 
	int counter = 0;
	// music allowed
	boolean playMusic = false;

	@Override
	public void create () {
		camera= new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		camera.setToOrtho(true, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		
		pmap = new Pixmap(mapW, mapH, Format.RGBA8888);
		// much faster than drawing every ocean pixel in the for cycles in render()
		pmap.setColor(waterColor); //ocean
		pmap.fill();
		
		symbolsPmap = new Pixmap(mapW, mapH, Format.RGBA8888);
		
		renderSprite = new SpriteBatch();
		path = new ShapeRenderer();
		
		finished = false;
		screenshotDone = false;
		
		gen = new HeightmapGenerator(mapW, mapH, oceanShoreline, contourLineDistance);
		// initial generation - setting the mountain tops
		generateMap();
		
		System.out.println(Gdx.files.internal("map.mp3").file().getAbsolutePath());
		//TODO read config to disable/enable music on start
		mapMusic = Gdx.audio.newMusic(Gdx.files.internal("map.mp3"));
		mapMusic.setLooping(true);	
		if(playMusic){
			//TODO fix music pausing during final generation step/render 
			mapMusic.play();
		}
	}
	
	void generateMap(){
		intmap = gen.getGeneratedMap3(generatorStep);
		
		int lastHeight = gen.getLastHeight();
		if(lastHeight == Integer.MAX_VALUE){
			//final redraw phase - map has been resized to bigger dimensions
			pmap.setColor(waterColor); //ocean
			pmap.fill();
		}

		for(int i = 0; i<intmap.length; i++) {
			for(int j=0; j<intmap[i].length; j++) {
				// Reduce the number of drawPixel calls by only drawing the pixels altered in last generation iteration 
				if(intmap[i][j]-1 <= lastHeight){ 	// Danger! potentially Integer.MAX_VALUE in lastHeight
					// Land
					if(intmap[i][j] > oceanShoreline) {
						// scale color with height
						float mp = ((intmap[i][j] - oceanShoreline)/contourLineDistance) / 60f;
						pmap.setColor((189/255f -mp), (155/255f -mp), (117/255f -mp), 1.0f); //lowland
						pmap.drawPixel(i, j);
					// Inland water body
					} else if(intmap[i][j] < 0){
						// needs different color than ocean -- it would draw wrong shoreline otherwise
						pmap.setColor(21/255f, 143/255f, 184/255f, 1.0f);
						pmap.drawPixel(i, j);
					}
				}
			}
		}
		if(map != null){
			map.dispose();
		}
		map = new Texture(pmap);
	}
	
	/*
	 * Makes a filter texture, that enhances water-land and land-land borders
	 */
	void doMagic(){
        Color baseColor = new Color(0.1f, 0.1f, 0.1f, 0.15f);
        LineFilter lif = new LineFilter(pmap, baseColor);
        iterator : for (int i=0; i<pmap.getWidth(); i++){
        	for(int j=0; j<pmap.getHeight(); j++){
        		// searches for the ocean pixel, to be used as color reference
        		// could possibly rewrite and provide color to the filter instead 
        		if(pmap.getPixel(i, j) == waterColor){
        			lif.setWaterPixel(i, j);
        			break iterator;
        		}
        	}
        }
        int lineWidth = 10;
        Pixmap drawing = lif.renforce(lif.filter(false),lineWidth,0.1f);
        filter = new Texture(drawing);
	}
	
	/*
	 * Makes an X mark and path to the treasure
	 */
	void addStartEndLocations(){
		int symbolSize = 7;
		
		int targetPosition = gen.getRandomPointNearOriginPoints(mapH/25);
		int targetX = gen.idToX(targetPosition);
		int targetY = gen.idToY(targetPosition);
		symbolsPmap.setColor(0.2f, 0.2f, 0.2f, 1);

		// render the X
		symbolsPmap.fillTriangle(targetX+symbolSize+symbolSize/3, targetY+symbolSize-symbolSize/3, targetX-symbolSize+symbolSize/3, targetY-symbolSize-symbolSize/3, targetX-symbolSize-symbolSize/3, targetY-symbolSize+symbolSize/3);
		symbolsPmap.fillTriangle(targetX+symbolSize+symbolSize/3, targetY+symbolSize-symbolSize/3, targetX+symbolSize-symbolSize/3, targetY+symbolSize+symbolSize/3, targetX-symbolSize-symbolSize/3, targetY-symbolSize+symbolSize/3);
		symbolsPmap.fillTriangle(targetX-symbolSize+symbolSize/3, targetY+symbolSize+symbolSize/3, targetX+symbolSize-symbolSize/3, targetY-symbolSize-symbolSize/3, targetX+symbolSize+symbolSize/3, targetY-symbolSize+symbolSize/3);
		symbolsPmap.fillTriangle(targetX-symbolSize+symbolSize/3, targetY+symbolSize+symbolSize/3, targetX-symbolSize-symbolSize/3, targetY+symbolSize-symbolSize/3, targetX+symbolSize-symbolSize/3, targetY-symbolSize-symbolSize/3);

		// generate ship landing point
		int landingPosition = gen.getRandomShorePoint();
		targetX = gen.idToX(landingPosition);
		targetY = gen.idToY(landingPosition);

		// generate the path from treasure to landing point 
		pathPoints = gen.getPathToShore(targetPosition, landingPosition);

		/* spline magic */
		// variable k to keep dash length of the dash line apx. the same 
		int k = 250*pathPoints.length; //increase k for more fidelity to the spline
		// points forming spline
	    points = new Vector2[k];
	    // duplicate initial and final point -- dunno why, was in the manual
	    Vector2 cp[] = new Vector2[pathPoints.length/2 + 2];
	    cp[0] = new Vector2(pathPoints[0], pathPoints[1]);
	    for(int i=0; i<pathPoints.length/2; i++){
	    	cp[i+1] = new Vector2(pathPoints[i*2], pathPoints[i*2+1]);
	    }
	    cp[cp.length-1] = new Vector2(pathPoints[pathPoints.length-2], pathPoints[pathPoints.length-1]);

	    // the magic; false to keep the end points not connected
        CatmullRomSpline<Vector2> myCatmull = new CatmullRomSpline<Vector2>( cp, false);
        for(int i = 0; i < k; ++i)
        {
            points[i] = new Vector2();
            myCatmull.valueAt(points[i], ((float)i)/((float)k-1));
        }

        symbols = new Texture(symbolsPmap);
	}
	
	/*
	 * Creates the screenshot file. Doesn't overwrite, makes different filename instead.
	 * 
	 * not working in HTML5
	 */
	void exportPng(){
		FileHandle handle = new FileHandle("map/map-new.png");
		int tries = 1;
		while(handle.exists()) {
			handle = new FileHandle("map/map-new" + tries + ".png");
			tries++;
		}
        Pixmap pixmapScreenshot = getScreenshot(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);
        
		PixmapIO.writePNG(handle, pixmapScreenshot);
	}
	
	/*
	 * 
	 * not working in HTML5
	 */
    private static Pixmap getScreenshot(int x, int y, int w, int h, boolean yDown){
        final Pixmap pixmap = ScreenUtils.getFrameBufferPixmap(x, y, w, h);

        if (yDown) {
            // Flip the pixmap upside down
            ByteBuffer pixels = pixmap.getPixels();
            int numBytes = w * h * 4;
            byte[] lines = new byte[numBytes];
            int numBytesPerLine = w * 4;
            for (int i = 0; i < h; i++) {
                pixels.position((h - i - 1) * numBytesPerLine);
                pixels.get(lines, i * numBytesPerLine, numBytesPerLine);
            }
            pixels.clear();
            pixels.put(lines);
            pixels.clear();
        }

        return pixmap;
    }

	
	

	@Override
	public void render () {
		camera.update();
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		renderSprite.begin();
		renderSprite.draw(map, 0, 0,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		if(filter != null && finished){
			renderSprite.draw(filter, 0, 0,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		}
		if(symbols != null && finished){
			renderSprite.draw(symbols, 0, 0,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		}

		renderSprite.end();
		
		if(pathPoints != null && finished){
			// to flip the axis
			path.setProjectionMatrix(camera.combined);
            path.setAutoShapeType(true);
            path.setColor(0.4f, 0.5f, 0.5f, 1);
            path.begin();
            for(int i = 0; i < points.length-2; ++i)
            {
                path.rectLine(points[i], points[i+1],3);
                // make it dashed
            	if(i%250 > 125){
            		i += 125;
            	}
            }
            path.end();
		}

		if(counter%1 == 0 && !gen.isDone()){
			generateMap();
		}
		//must be before next if clause
		if(gen.isDone() && finished && !screenshotDone){
			exportPng();
			screenshotDone = true;
		}
		if(gen.isDone() && !finished){
			doMagic();
			addStartEndLocations();
			finished = true;
		}
		
		/* process user input */
		if (Gdx.input.isKeyJustPressed(Keys.LEFT)){
			// generate new map
			dispose();
			create();
		}
		if (Gdx.input.isKeyJustPressed(Keys.RIGHT)){
			// generate new map
			dispose();
			create();
		}
		if (Gdx.input.isKeyJustPressed(Keys.ESCAPE)){
			Gdx.app.exit();
		}
		if (Gdx.input.isKeyJustPressed(Keys.M)){
			// turn on/off music
			if(mapMusic.isPlaying()){
				mapMusic.stop();
				playMusic = false;
			}
			else{
				mapMusic.play();
				playMusic = true;
			}
			
		}

	}
	
	@Override
	public void dispose() {
		map.dispose();
		filter.dispose();
		symbols.dispose();

		pmap.dispose();
		symbolsPmap.dispose();
		
		renderSprite.dispose();
		mapMusic.dispose();
		path.dispose();
	}
}
