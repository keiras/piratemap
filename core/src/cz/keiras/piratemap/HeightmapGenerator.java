package cz.keiras.piratemap;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Stack;

import za.co.luma.geom.Vector2DDouble;
import za.co.luma.math.sampling.Sampler;
import za.co.luma.math.sampling.UniformPoissonDiskSampler;


/**
 * Created by Keiras <kryl.martin@centrum.cz>
 */
public class HeightmapGenerator {

	int startingPoints = 30;
	int startingHeight = 57;  
	/* 0-100% chance of the neighboring cell having lower height than the original cell */
	double probabilityOfMovingDown = 0.75f;
	/* Maximal reduction of height after probabilityOfMovingDown roll fails */
	int movingDownBy = 2;
	
	/* Number of pixels in row/column represented as 1 pixel in reduced matrix */
	int step;
	/* Landscape generation completion flag  */
	boolean done;
	/* Height used as baseline in previous step of landscape generation  */
	int lastHeight;
	
	/* Final picture dimensions */
	int width;
	int height;
	/* highest possible height for ocean water pixels */
	int oceanShoreline;
	/* Each contour line represents contourLineDistance change in height */
	int contourLineDistance;
	
	/* Instance of Random for landscape generator */
	Random rnd;
	/* Pooled list for general purposes */
	LinkedList<Integer> listOfPoints;
	/* Initially generated points (local maximums). Uses coordinates on final (not reduced) map. */
	LinkedList<Integer> originPoints;
	
	/* final heightmap */
	int[][] intmap;
	/* heightmap of reduced dimensions*/
	int[][] smallGrid;
	/* bitmap containing id of water body located on the coordinates */
	int[][] smallGridWaterBodies;
	
	/* Currently unseeded,  */
	public HeightmapGenerator(int width, int height, int oceanShoreline, int contourLineDistance){
		this.width = width;
		this.height = height;
		this.oceanShoreline = oceanShoreline;
		this.contourLineDistance = contourLineDistance;
		rnd = new Random();
		intmap = new int[this.width][this.height];
		lastHeight = Integer.MAX_VALUE;
		done = false;
		listOfPoints = new LinkedList<Integer>();
		originPoints = new LinkedList<Integer>();
	}
	
	/* Checks, if landscape generation is complete*/
	public boolean isDone(){
		return done;
	}
	
	/* Last height used as baseline for terrain spilling */
	public int getLastHeight(){
		return lastHeight;
	}
	
	/* 
	 * Returns id of random point from 2*distance x 2*distance square area with random origin point in center.
	 * Returned point is above ocean shoreline.
	 * Returns random origin point, if no valid point is found after 500 attempts.
	 */
	public int getRandomPointNearOriginPoints(int distance){
		int maxAttempts = 500;

		int randomOrigin = originPoints.get(rnd.nextInt(originPoints.size()));
		if(distance <= 0){
			return randomOrigin;
		}
		int antistuck = 0;
		while(antistuck < maxAttempts){
			antistuck++;
			int newX = distance - rnd.nextInt(distance*2) + idToX(randomOrigin);
			int newY = distance - rnd.nextInt(distance*2) + idToY(randomOrigin);
			if(newX >= 0 && newY >= 0 && newX < intmap.length && newY < intmap[newX].length && intmap[newX][newY] > oceanShoreline){
				return xyToId(newX, newY);
			}
		}
		return randomOrigin;
	}
	
	/* 
	 * Returns id of pseudo-random water point on ocean shoreline.  
	 * Procedure: 
	 * 		Get random ocean pixel's coordinates.	 
	 * 		Find the first shore pixel (if any) from left/right border with gained Y coordinate and
	 * 		add the last water pixels before land to list.
	 * 		Find the first shore pixel (if any) from top/bottom border with gained X coordinate and
	 * 		add the last water pixels before land to list.
	 * 		Return random point from list.
	 * 
	 * Potentially endless cycle if no non-ocean pixel exists 
	 * or slow execution if only few non-ocean pixels exist. 
	 * */
	public int getRandomShorePoint(){
//		List<Integer> candidates = new ArrayList<Integer>();
		
		while(true){
			listOfPoints.clear();
			int randomPoint = rnd.nextInt(smallGrid.length*smallGrid[0].length);
	
			int x = idToX(randomPoint, true);
			int y = idToY(randomPoint, true);
			
			for(int i=0; i<smallGrid.length; i++){
				if(smallGrid[i][y] > oceanShoreline){
					if(i>0){
						listOfPoints.add(xyToId((i-1)*step, y*step));
					}
					break;
				}
			}
			for(int i=smallGrid.length-1; i>=0; i--){
				if(smallGrid[i][y] > oceanShoreline){
					if(i<smallGrid.length-1){
						listOfPoints.add(xyToId((i+1)*step, y*step));
					}
					break;
				}
			}
			
			for(int i=0; i<smallGrid[0].length; i++){
				if(smallGrid[x][i] > oceanShoreline){
					if(i>0){
						listOfPoints.add(xyToId(x*step, (i-1)*step));
					}
					break;
				}
			}
			for(int i=smallGrid[0].length-1; i>=0; i--){
				if(smallGrid[x][i] > oceanShoreline){
					if(i<smallGrid.length-1){
						listOfPoints.add(xyToId(x*step, (i+1)*step));
					}
					break;
				}
			}
			
			if(listOfPoints.size()>0){	
				// need to clean pooled list object
				int answer = listOfPoints.get(rnd.nextInt(listOfPoints.size()));
				listOfPoints.clear();
				return answer;
			}
		}
	}
	
	/* Generates landscape by randomly creating several elevated points and spilling land to surrounding points.
	 * Procedure:
	 * 		Create several elevated points, if not yet done.
	 * 		Find all points with lastHeight on reduced map.
	 * 		From every point move in N, S, W and E directions
	 * 			begin with origin point's height
	 *	 		after each step roll for height reduction chance and set visited pixel's height to current height, if not set higher already
	 * 			stop moving in direction, when current height is zero.
	 * 		Decrement lastHeight.	 
	 * 
	 * Step parameter sets the multitude of reduction - "step = 5" makes 1 pixel in reduced map for every 5 pixels of original map.
	 * Uses bilinear-ish interpolation and cellular automata to smooth out edges. 
	 * 
	 *  */
	public int[][] getGeneratedMap3(int step){
		this.step = step;
		if(!done){
			if(lastHeight == Integer.MAX_VALUE){
				//initial points distribution
				for (int i=0; i<startingPoints; i++){
					int pixelsW = width/step;
					int pixelsH = height/step;
					if(smallGrid == null){
						// danger when replacing "dispose();create()" sequence with something more elaborate to generate new landscape
						smallGrid = new int[pixelsW][pixelsH];
						smallGridWaterBodies = new int[pixelsW][pixelsH];
					}
					// force points to be near center
					int rndX = (pixelsW/4 + rnd.nextInt(pixelsW/2));
					int rndY = (pixelsH/4 + rnd.nextInt(pixelsH/2));
					// initial height is from range (startingHeight/2; startingHeight)
					smallGrid[rndX][rndY] = rnd.nextInt(startingHeight/2)+startingHeight/2;
					originPoints.add(xyToId(rndX*step,rndY*step));
				}
				//TODO change to highest value from initial generation - no big deal atm
				lastHeight = startingHeight;
			}
			//not sure why I made this here. It should't be reachable by normal logic.
			else if(lastHeight <= 0){
				done = true;
			}
			else{
				getPoints(listOfPoints, lastHeight, smallGrid);
				while(!listOfPoints.isEmpty()){
					int id = listOfPoints.removeFirst();
					// go west
					int value = lastHeight;
					int y = idToY(id);
					for(int x = idToX(id); x>=0; x--){
						if(value <= 0){
							break;
						}
						while(rnd.nextDouble()<probabilityOfMovingDown){
							value -= rnd.nextInt(movingDownBy)+1;
						}
						if(smallGrid[x][y] < value){
							smallGrid[x][y] = value;
							if(value == lastHeight){
								listOfPoints.add(xyToId(x,y));
							}
						}
					}
					// go east
					value = lastHeight;
					y = idToY(id);
					for(int x = idToX(id); x<smallGrid.length; x++){
						if(value <= 0){
							break;
						}
						while(rnd.nextDouble()<probabilityOfMovingDown){
							value -= rnd.nextInt(movingDownBy)+1;
						}
						if(smallGrid[x][y] < value){
							smallGrid[x][y] = value;
							if(value == lastHeight){
								listOfPoints.add(xyToId(x,y));
							}
						}
					}
					
					// go north
					value = lastHeight;
					int x = idToX(id);
					for(y = idToY(id); y>=0; y--){
						if(value <= 0){
							break;
						}
						while(rnd.nextDouble()<probabilityOfMovingDown){
							value -= rnd.nextInt(movingDownBy)+1;
						}
						if(smallGrid[x][y] < value){
							smallGrid[x][y] = value;
							if(value == lastHeight){
								listOfPoints.add(xyToId(x,y));
							}
						}
					}
					// go south
					value = lastHeight;
					x = idToX(id);
					for(y = idToY(id); y<smallGrid[x].length; y++){
						if(value <= 0){
							break;
						}
						while(rnd.nextDouble()<probabilityOfMovingDown){
							value -= rnd.nextInt(movingDownBy)+1;
						}
						if(smallGrid[x][y] < value){
							smallGrid[x][y] = value;
							if(value == lastHeight){
								listOfPoints.add(xyToId(x,y));
							}
						}
					}
					
				}
				lastHeight--;	
			}
			if(lastHeight <= 0){
				//polish the landscape				
				cleanBorders(smallGrid);
				findInnerWaterBodies();
				extendSmallGrid(smallGrid, step);
				interpolate2(intmap, step);
				differentiateInlandWater();
				
				done = true;
				lastHeight = Integer.MAX_VALUE; //to let drawer refresh all the pixels
				// returns the full-sized map
				return intmap;
			}
		}
		//returns reduced map
		return smallGrid;
	}

	
	/*
	 * Finds water bodies, that is points with value <= oceanshoreline. 
	 * Each water body and identified by integer value.
	 *  
	 */
	void findInnerWaterBodies(){
		int waterBodyTreshold = 20;
		
		//TODO check for land in corner
		int cols = smallGrid.length;
		int rows = smallGrid[0].length;

		// adds corner points to stack as they are MOST LIKELY ocean 
		// ocean is the only water body with a possibility of being a set of disconnected points
		Stack<Integer> st = new Stack<Integer>();
		st.push(xyToId(0,0,true));
		st.push(xyToId(0,rows-1,true));
		st.push(xyToId(cols-1,0,true));
		st.push(xyToId(cols-1,rows-1,true));

		// number of pixels in each water body
		ArrayList<Integer> waterBodySize = new ArrayList<Integer>();
		waterBodySize.add(0); //zero index - could be changed to count zeroes

		// counter for water body id
		int colorCounter = 1;
		waterBodySize.add(recolorNeighbourWater(st, colorCounter++));
		
		for(int i=0; i<cols; i++){
			for(int j=0; j<rows; j++){
				// find water pixels not yet asigned to water body
				if(smallGridWaterBodies[i][j] == 0 && smallGrid[i][j] <= oceanShoreline){
					st.push(xyToId(i,j,true));
					waterBodySize.add(recolorNeighbourWater(st, colorCounter++));
				}
			}
		}
		
		// clean up the map by removing bodies with small number of water pixels 
		for(int i=0; i<cols; i++){
			for(int j=0; j<rows; j++){
				if(smallGridWaterBodies[i][j] > 1 && waterBodySize.get(smallGridWaterBodies[i][j]) < waterBodyTreshold){
					smallGrid[i][j] = oceanShoreline+1;
					smallGridWaterBodies[i][j] = 0;
				}
			}
		}

	}
	
	/*
	 * Changes indexes for water points connected to points from input stack to input value in smallGridWaterBodies matrix.
	 */
	int recolorNeighbourWater(Stack<Integer> st, int number){
		int cols = smallGrid.length;
		int rows = smallGrid[0].length;
		
		int count = 0;

		// filling algorithm to find neighboring water pixels
		while (!st.isEmpty()){
			int point = st.pop();
			smallGridWaterBodies[idToX(point,true)][idToY(point,true)] = number;
			count++;
			// check neighboring points
			int[] points = {point - 1, point + 1, point + cols, + point - cols};
			for(int p : points){
				if(p < 0 || p >= cols*rows){
					continue;
				}
				if(smallGrid[idToX(p,true)][idToY(p,true)] <= oceanShoreline && smallGridWaterBodies[idToX(p,true)][idToY(p,true)] == 0){
					st.push(p);
				}
			}
		}
		return count;
	}
	
	
	/*
	 * Changes indexes for water points connected to points from input stack to input value in input matrix.
	 */
	int recolorNeighbourWater(Stack<Integer> st, int number, int[][] target){
		int cols = target.length;
		int rows = target[0].length;
		
		int count = 0;
		
		// filling algorithm to find neighboring water pixels
		while (!st.isEmpty()){
			int point = st.pop();
			target[idToX(point)][idToY(point)] = number;
			count++;
			// check neighboring points
			int[] points = {point - 1, point + 1, point + cols, + point - cols};
			for(int p : points){
				if(p < 0 || p >= cols*rows){
					continue;
				}
				if(target[idToX(p)][idToY(p)] <= oceanShoreline && target[idToX(p)][idToY(p)] >= 0){
					st.push(p);
				}
			}
		}
		return count;
	}
	
	/* Copies data from smaller input matrix into intmap matrix. 
	 * Writes onto target coordinates, for which x%step == 0 and y%step == 0 
	 * Beware unchecked OutOfBounds exception. 
	 */
	void extendSmallGrid(int[][] small, int step){
		for(int i=0; i<small.length; i++){
			for(int j=0; j<small[i].length; j++){
				intmap[i*step][j*step] = small[i][j];
			}
		}
	}
	
	// FIXME bugged south and east borders (or prevent island touching border)
	/*
	 * Interpolates values in target grid for coordinates, where x%step != 0 and y%step != 0.
	 * Bilinear-ish interpolation algorithm.
	 * 
	 * Beware of rounding to integer
	 */
	void interpolate2(int[][] grid, int step){
		if(step > 1){
			//interpolate horizontally every step-th row
			for(int i=0; i<(grid.length-1)/step; i++){
				for(int j=0; j<(grid[i].length)/step; j++){
					int diff = grid[i*step][j*step]-grid[(i+1)*step][j*step];
					for(int k=1; k<step; k++){
						grid[i*step+k][j*step] = grid[i*step][j*step] - Math.round((float)k*diff/step);
					}
				}				
			}
			
			//interpolate vertically every step-th column
			for(int i=0; i<(grid.length)/step; i++){
				for(int j=0; j<(grid[i].length-1)/step; j++){
					int diff = grid[i*step][j*step]-grid[i*step][(j+1)*step];
					for(int k=1; k<step; k++){
						grid[i*step][j*step+k] = grid[i*step][j*step] - Math.round((float)k*diff/step);
					}
				}				
			}
			
			//interpolate inner space by choosing max value from values interpolated horizontally and vertically
			for(int i=0; i<(grid.length-1)/step; i++){
				for(int j=0; j<(grid[i].length-1)/step; j++){
					for(int kx=1; kx<step; kx++){
						for(int ky=1; ky<step; ky++){
							int diffH = grid[i*step][j*step+ky]-grid[(i+1)*step][j*step+ky];
							int diffV = grid[i*step+kx][j*step]-grid[i*step+kx][(j+1)*step];
							int valueH = grid[i*step][j*step+ky] - Math.round((float)kx*diffH/step);
							int valueV = grid[i*step+kx][j*step] - Math.round((float)ky*diffV/step);
							grid[i*step+kx][j*step+ky] = (valueH + valueV)/2;
						}
						
					}

				}				
			}
		
		}
	}
	
	/* Set all inland water bodies pixels' value to -1  */
	void differentiateInlandWater(){
		Stack<Integer> st = new Stack<Integer>();
		ArrayList<Integer> lakeSize = new ArrayList<Integer>();
		for(int i=0; i<smallGridWaterBodies.length; i++){
			for(int j=0; j<smallGridWaterBodies[0].length; j++){
				if(smallGridWaterBodies[i][j] > 1 && intmap[i*step][j*step] >= 0){
					st.push(xyToId(i*step, j*step));
					lakeSize.add(recolorNeighbourWater(st, -1, intmap));
				}
			}
		}
		System.out.println(lakeSize.toString());
	}


	/*
	 * Smoothes island shoreline and 2 contour lines by cellular automata-ish logic.
	 * Looks at 3x3 square, if number of target pixels < 5, recolor the center pixel.
	 * Pixels are recolored in batch, for automata logic to work properly.
	 * 
	 */
	void cleanBorders(int[][] grid){
		int liveTreshold = 5;
		boolean done = false;
		
		// TODO pool the linked lists?
		LinkedList<Integer> listOfPoints2 = new LinkedList<Integer>();
		LinkedList<Integer> listOfPoints3 = new LinkedList<Integer>();
		
		while(!done){
			int[] stats = new int[10];

			for(int i=1; i<grid.length-1; i++){
				for(int j=1; j<grid[i].length-1; j++){
					if(grid[i][j] > oceanShoreline && grid[i][j] < oceanShoreline+contourLineDistance){
						int count = 0;
						for(int x=-1; x<=1; x++){
							for(int y=-1; y<=1; y++){
								if(grid[i+x][j+y]>oceanShoreline){
									count++;
								}
							}
						}
						if(count < liveTreshold){
							listOfPoints.add(xyToId(i,j));
						}
					}
					
					if(grid[i][j] >= oceanShoreline+contourLineDistance && grid[i][j] < oceanShoreline+2*contourLineDistance){
						int count = 0;
						for(int x=-1; x<=1; x++){
							for(int y=-1; y<=1; y++){
								if(grid[i+x][j+y]>oceanShoreline+contourLineDistance){
									count++;
								}
							}
						}
						
						stats[count]++;
						
						if(count < liveTreshold){
							listOfPoints2.add(xyToId(i,j));
						}
					}
					
					if(grid[i][j] >= oceanShoreline+2*contourLineDistance && grid[i][j] < oceanShoreline+3*contourLineDistance){
						int count = 0;
						for(int x=-1; x<=1; x++){
							for(int y=-1; y<=1; y++){
								if(grid[i+x][j+y]>oceanShoreline+2*contourLineDistance){
									count++;
								}
							}
						}
						
						stats[count]++;
						
						if(count < liveTreshold){
							listOfPoints3.add(xyToId(i,j));
						}
					}
				}
			}

//			System.out.println("histogram: " + Arrays.toString(stats));
	
			// finish loop when there are no longer any pixels to recolor.
			// I BELIEVE this is enough to guarantee finite loop.
			if(listOfPoints.size() == 0 && listOfPoints2.size() == 0 && listOfPoints3.size() == 0){
				done = true;
			}
			
			
			while(!listOfPoints.isEmpty()){
				int id = listOfPoints.removeFirst();
				grid[idToX(id)][idToY(id)] = oceanShoreline;
				
			}
			
			while(!listOfPoints2.isEmpty()){
				int id = listOfPoints2.removeFirst();
				grid[idToX(id)][idToY(id)] = oceanShoreline+contourLineDistance-1;
				
			}
			
			while(!listOfPoints3.isEmpty()){
				int id = listOfPoints3.removeFirst();
				grid[idToX(id)][idToY(id)] = oceanShoreline+2*contourLineDistance-1;
				
			}
			
		}
	}
	
	/* Returns a set of points with specified height in the input grid */
	private void getPoints(LinkedList<Integer> al, int height, int[][] grid){
		for(int i=0; i<grid.length; i++){
			for(int j=0; j<grid[i].length; j++){
				if(grid[i][j] == height){
					al.add(xyToId(i,j));
				}
			}
		}
		return;
	}
	
	/*
	 * utilities to convert id <-> x,y
	 */
	public int xyToId(int x, int y){
		return x+y*width;
	}	
	public int idToX(int id){
		return id%width;
	}
	public int idToY(int id){
		return id/width;
	}
	public int xyToId(int x, int y, boolean smallGridUsed){
		if(smallGridUsed){
			return x+y*smallGrid.length;
		}
		return x+y*width;
	}	
	public int idToX(int id, boolean smallGridUsed){
		if(smallGridUsed){
			return id%smallGrid.length;
		}
		return id%width;
	}
	public int idToY(int id, boolean smallGridUsed){
		if(smallGridUsed){
			return id/smallGrid.length;
		}
		return id/width;
	}
	
	/*
	 * Returns an ordered list of points, that should be traversed to create path between input points.
	 * Initial and final points are included in the list.
	 * 
	 * Uses Poisson disk sampling and simple greedy approach in every step.
	 * Extra cleanup logic is used, since the greedy algorithm likes to create loops from time to time.
	 * 
	 * Path can technically lead over water in rare cases, since only the reference points are tested for height.
	 * Testing all the points along the path seemed like a pain, since I don't connect reference points via straight line.
	 * Also probably not worth the computing time.
	 * 
	 */
	public int[] getPathToShore(int origin, int exit){
		/* maximum distance between two consecutive points in path */
		int maxDistance = 30;
		/* distance between points in poisson disc */
		int poissonSpreadDistance = 5;
		
		ArrayList<Integer> path = new ArrayList<Integer>();
		int lastX = idToX(origin);
		int lastY = idToY(origin);
		int exitX = idToX(exit);
		int exitY = idToY(exit);
		path.add(lastX);
		path.add(lastY);
		
		// TODO deal with Vector2DDouble to be able to compile HTML5 version
		Sampler<Vector2DDouble> sampler = new UniformPoissonDiskSampler(0, 0, intmap.length, intmap[0].length, poissonSpreadDistance);
		List<Vector2DDouble> pointList = sampler.sample();

		List<Vector2DDouble> nearPoints = new ArrayList<Vector2DDouble>();
		// Can be different than path, because of loop cleaning process. Removed points from loops remains in this list.
		List<Vector2DDouble> pointsVisited = new ArrayList<Vector2DDouble>();
		Vector2DDouble lastPoint = new Vector2DDouble(lastX, lastY);
		while(true){
			nearPoints.clear();
			for(Vector2DDouble point : pointList){
				if(point.distance(lastX, lastY) < maxDistance){
					nearPoints.add(point);
				}
			}	
			// TIL Double.MIN_VALUE is actually a positive number
			double bestScore = Double.NEGATIVE_INFINITY;
			Vector2DDouble bestPoint = null;

			for(Vector2DDouble point : nearPoints){
				if(pointsVisited.contains(point)){
					continue;
				}
				/* No water points */
				if(intmap[(int)point.x][(int)point.y] <= oceanShoreline){
					continue;
				}
				
				double score=1;
				// used to consider distance between step n-1 and n-2 to motivate algo not to turn ~180�� that often. Didn't work out.
			//	double score = point.distance(preLastX, preLastY);
				// score is lowered if near (or at) water pixels. This is to create more interesting path that doesn't overlap shoreline.
				if (intmap[(int)point.x][(int)point.y]-oceanShoreline < 6){
					score *= 0.5;
				}
				// extra penalty for being even closer to water
				else if(intmap[(int)point.x][(int)point.y]-oceanShoreline < 3){
					score *= 0.5;
				}
				// moves that brings us closer to goal are better -- this is the main culprit for loops
				score *= lastPoint.distanceSq(exitX, exitY)/point.distanceSq(exitX, exitY);
				// ratio of previous/next height. -> we prefer to walk down the hill rather then up
				// note: there is higher malus for moving up in lower regions, than in upper regions
				// note2: tweaking oceanShoreline constant heavily influences this calculation
				// "-2" tolerance magic number makes more interesting path, since the algo is a bit more willing to move up in lower regions
				// TODO should probably negate the "-2" effect on moving down and steying on the same level
				score *= (double)intmap[lastX][lastY]/(double)(intmap[(int)point.x][(int)point.y]-2);
								
				if(score > bestScore){
					bestScore = score;
					bestPoint = point;
				}
			}
			
			// could be reached when there are no unvisited points in range
			if(bestPoint == null){
				break;
			}
			lastX = (int) bestPoint.x;
			lastY = (int) bestPoint.y;
			lastPoint = bestPoint;
			pointsVisited.add(bestPoint);		
			path.add(lastX);
			path.add(lastY);

			// finish the journey if target is in reach
			if(bestPoint.distance(exitX, exitY) < maxDistance){
				path.add(exitX);
				path.add(exitY);
				break;
			}
			
		}
		
		
		//test for loop + remove loops

	    int i=0;
	    while(i < path.size()){
	    	int x = path.get(i);
	    	int y = path.get(i+1);
	    	i += 2;
	    	for(int j = path.size()-1; j>i+2; j -=2){
	    		if (new Vector2DDouble(path.get(j-1), path.get(j)).distance(x, y) < maxDistance){
	    			path.subList(i, j-1).clear();
		    		System.out.println("Removed "+ (j-1-i)/2 +" points from loop. Now has: " +path.size()/2 +" points");
		    		break;
	    		}
	    	}
	    	
	    }

		// transform list to int[]
	    int[] ret = new int[path.size()];
	    Iterator<Integer> iterator = path.iterator();
	    for (i = 0; i < ret.length; i++)
	    {
	        ret[i] = iterator.next().intValue();
	    }
	    return ret;
	}
	
}
