package cz.keiras.piratemap.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import cz.keiras.piratemap.PirateMap;

public class HtmlLauncher extends GwtApplication {

        @Override
        public GwtApplicationConfiguration getConfig () {
        	GwtApplicationConfiguration cfg = new GwtApplicationConfiguration(1024, 768);
        	return cfg;
        }

        @Override
        public ApplicationListener getApplicationListener () {
                return new PirateMap();
        }
}