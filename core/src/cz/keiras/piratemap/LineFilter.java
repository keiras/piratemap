package cz.keiras.piratemap;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;

/*
 * Created by simon on 05/12/2015.
 * 
 * Modified by Keiras.
 */
public class LineFilter {

    private Pixmap im;
    private Color color;
    private int waterPixelX;
    private int waterPixelY;
    
    private int waterColor;

    public LineFilter(Pixmap image, Color baseColor){
        this.im = image;
        this.color = baseColor;
        waterPixelX = 0;
        waterPixelY = 0;
    }

    public void setWaterPixel(int x, int y){
    	waterPixelX = x;
    	waterPixelY = y;
    	waterColor = im.getPixel(waterPixelX,waterPixelY);
    }


    /*
     * Creates filter using color data from Pixmap inputed in constructor.
     * - has "no" (black) pixel if relative 3x3 pixels all have the same color.
     * - has "half" (dark grey) pixel if relative 3x3 pixels have mixed colors, but not waterColor.
     * - has "yes" (baseColor from constructor) pixel if relative 3x3 have mixed colors, one of them waterColor.  
     * 
     * Current implementation considers only the first differently colored pixel in relation to the center pixel.
     * Therefore, it might not work, if there is contour line directly next to the sea shore.   
     * 
     * "noisy" parameter makes contour lines noisy by omitting some pixels and leaving "no" pixel instead. 
     * */
    public Pixmap filter(boolean noisy){
        int yes = color.toIntBits();
        int half = new Color(30f/255,30f/255,30f/255,1f).toIntBits();
        int no = new Color(0,0,0,1f).toIntBits();
        
        Pixmap gray = new Pixmap(im.getWidth(),im.getHeight(), Format.RGBA8888);
        gray.setColor(no);
        gray.fill();

        for(int x = 1; x < im.getWidth()-1; x++){
            for(int y = 1; y < im.getHeight()-1; y++){
                //Look at the neighbours
                testNeigh : for(int i=-1;i<2;i++){
                    for(int j=-1;j<2;j++){
                        if(i==0 && j==0){continue;}
                        //Is there diff in neigbouring pixel's colors?
                        if(im.getPixel(x,y) != im.getPixel(x+i,y+j)){
                            if(im.getPixel(x,y) == waterColor || im.getPixel(x+i,y+j) == waterColor){
                            	//Land-water border
                                gray.drawPixel(x,y,yes);
                                
                            } else {
                            	//Land-land height contour line
                                if(!noisy || Math.random() < 0.9) {
                                    gray.drawPixel(x, y, half);
                                }
                            }
                            break testNeigh;
                        }
                    }
                }
            }
        }

        return gray;
    }

    /*
     * Iterates the renforce() call. Increases noiseFactor constantly in each call. 
     * 
     */
    public Pixmap renforce(Pixmap gray, int count, float noiseFactor){

        for(int i = 0; i < count; i++){
            gray = renforce(gray, noiseFactor * i);
        }
        return gray;
    }


    /*
     * Recolors some water pixels neighboring differently colored pixel
     * 
     */
    public Pixmap renforce(Pixmap gray, float noiseFactor){
        int yes = Color.rgba8888(color);
        Pixmap gray2 = new Pixmap(gray.getWidth(), gray.getHeight(), Format.RGBA8888);
        // duplicates input Pixmap to new Pixmap
        gray2.drawPixmap(gray, 0, 0);
        
        for(int x = 1; x < im.getWidth()-1; x++){
            for(int y = 1; y < im.getHeight()-1; y++){
                //Look at the neighbours
                testNeigh : for(int i=-1;i<2;i++){
                    for(int j=-1;j<2;j++){
                        if(i==0 && j==0){continue;}
                        if(gray.getPixel(x,y) != gray.getPixel(x+i,y+j) && im.getPixel(x,y) == waterColor){
                            if(Math.random() < 1.0 - noiseFactor) {
                                gray2.drawPixel(x, y, yes);
                            }
                            break testNeigh;
                        }
                    }
                }
            }
        }
        gray.dispose();
        return gray2;
    }
    
}
